//  DBConnection.js
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

var drivelist = require("drivelist");
var Promise = require("promise");
var fs = require("fs");
var xpath = require("xpath");
var dom = require("xmldom").DOMParser;
var mysql = require("mysql");
var pg = require("pg");
var mssql = require("mssql");
var PreparedStatement = require("./PreparedStatement.js");

function DBConnection(driverClassName, server, database, userName, password, port, callback) {
	var outClazz = null;
	var outServer = null;
	var outPort = 0;
	var outDatabase = null;
	var outConn = null;

	this._conn = null;
	this._clazz = null;
	this._server = null;
	this._port = 0;
	this._database = null;
	this._connecting = false;
	this._connected = false;
	this._preparedStatements = [];
	this._typesLookup = {};

	if(driverClassName !== undefined && server !== undefined && database !== undefined && userName !== undefined && password !== undefined)
	{
		var done = false;

		if(callback === undefined && port !== undefined)
		{
			callback = port;
			port = undefined;
		}
		this.ConnectWithUsernamePasswordServer(driverClassName, server, database, userName, password, port, callback);
	}
	else if(driverClassName !== undefined && server !== undefined && database === undefined)
	{
		var context = driverClassName;
		callback = server;
		var done = false;

		this.ConnectWithContext(context, callback);

	}
	else if(driverClassName === undefined)
	{
		return callback(null, this);
	}
	else
	{
		return callback(new Error("Unexpected args!"));
	}
}

DBConnection.prototype = (function() {
	var ParseMysqlUrlString = function(url, outInfo)
	{
		var idx;

		var hasColon = false;
		var hasSlash = false;
		var foundIdx = false;

		outInfo.host = "";
		outInfo.port = "3306";
		outInfo.db = "";

		for(idx = 0; idx < url.length; idx++)
		{
			var c = url.charAt(idx);

			if (c == ':' && !hasColon)
			{
				hasColon = true;
			}
			else if (' \t\n\r\v'.indexOf(c) > -1)
			{
			}
			else if (c == '/')
			{
				if (hasColon && !hasSlash)
				{
					hasSlash = true;
				}
				else if (hasColon && hasSlash)
				{
					foundIdx = true;
					break;
				}
				else
				{
					hasColon = false;
					hasSlash = false;
				}
			}
			else
			{
				hasColon = false;
				hasSlash = false;
			}
		}

		if (!foundIdx)
		{
			return false;
		}

		var curUrl = url.substring(idx + 1);
		var doHost = true;
		var doPort = false;
		var doDb = false;
		var parseFailed = false;

		for (idx = 0; idx < curUrl.length; idx++)
		{
			var c = curUrl.charAt(idx);

			if (doHost)
			{
				if (c == ':')
				{
					doHost = false;
					doPort = true;
					outInfo.port = "";
				}
				else if (c == '/')
				{
					doHost = false;
					doDb = true;
				}
				else
				{
					outInfo.host += c;
				}
			}
			else if (doPort)
			{
				if (doPort && c == '/')
				{
					doPort = false;
					doDb = true;
				}
				else
				{
					outInfo.port += c;
				}
			}
			else if (doDb)
			{
				if (c == '/')
				{
					parseFailed = true;
					break;
				}
				else if (c == '?')
				{
					break;
				}
				else
				{
					outInfo.db += c;
				}
			}
		}

		return !parseFailed;
	}

	var ParsePostgresqlUrlString = function(url, outInfo)
	{
		var idx;

		var hasColon = false;
		var hasSlash = false;
		var foundIdx = false;

		outInfo.host = "";
		outInfo.port = "5432";
		outInfo.db = "";

		for(idx = 0; idx < url.length; idx++)
		{
			var c = url.charAt(idx);

			if (c == ':' && !hasColon)
			{
				hasColon = true;
			}
			else if (' \t\n\r\v'.indexOf(c) > -1)
			{
			}
			else if (c == '/')
			{
				if (hasColon && !hasSlash)
				{
					hasSlash = true;
				}
				else if (hasColon && hasSlash)
				{
					foundIdx = true;
					break;
				}
				else
				{
					hasColon = false;
					hasSlash = false;
				}
			}
			else
			{
				hasColon = false;
				hasSlash = false;
			}
		}

		if (!foundIdx)
		{
			return false;
		}

		var curUrl = url.substring(idx + 1);
		var doHost = true;
		var doPort = false;
		var doDb = false;
		var parseFailed = false;

		for(idx = 0; idx < curUrl.length; idx++)
		{
			var c = curUrl.charAt(idx);

			if (doHost)
			{
				if (c == ':')
				{
					doHost = false;
					doPort = true;
					outInfo.port = "";
				}
				else if (c == '/')
				{
					doHost = false;
					doDb = true;
				}
				else
				{
					outInfo.host += c;
				}
			}
			else if (doPort)
			{
				if (doPort && c == '/')
				{
					doPort = false;
					doDb = true;
				}
				else
				{
					outInfo.port += c;
				}
			}
			else if (doDb)
			{
				if (c == '/')
				{
					parseFailed = true;
					break;
				}
				else if (c == '?')
				{
					break;
				}
				else
				{
					outInfo.db += c;
				}
			}
		}

		return !parseFailed;
	};

	var ParseSqlServerUrlString = function(url, outInfo, inOutUserNamePassword)
	{
		var idx = 0;

		var hasColon = false;
		var hasSlash = false;
		var foundIdx = false;

		outInfo.host = "";
		outInfo.port = "1433";
		outInfo.db = "";

		for(idx = 0; idx < url.length; idx++)
		{
			var c = url.charAt(idx);

			if (c == ':' && !hasColon)
			{
				hasColon = true;
			}
			else if (' \t\n\r\v'.indexOf(c) > -1)
			{
			}
			else if (c == '/')
			{
				if (hasColon && !hasSlash)
				{
					hasSlash = true;
				}
				else if (hasColon && hasSlash)
				{
					foundIdx = true;
					break;
				}
				else
				{
					hasColon = false;
					hasSlash = false;
				}
			}
			else
			{
				hasColon = false;
				hasSlash = false;
			}
		}

		if (!foundIdx)
		{
			return false;
		}

		var curUrl = url.substring(idx + 1);
		var doHost = true;
		var doPort = false;
		var parseFailed = false;

		for(idx = 0; idx < curUrl.length; idx++)
		{
			var c = curUrl.charAt(idx);

			if (doHost)
			{
				if (c == ':')
				{
					doHost = false;
					doPort = true;
					outInfo.port = "";
				}
				else if (c == '/')
				{
					parseFailed = true;
					break;
				}
				else if (c == ';')
				{
					break;
				}
				else
				{
					outInfo.host += c;
				}
			}
			else if (doPort)
			{
				if (doPort && c == '/')
				{
					parseFailed = true;
					break;
				}
				else if (c == ';')
				{
					break;
				}
				else
				{
					outInfo.port += c;
				}
			}
		}

		if (parseFailed)
		{
			return false;
		}

		curUrl = curUrl.substring(idx + 1);

		var getKey = true;
		var getValue = false;
		var getQuoteValue = false;
		var escaped = false;

		var key = "";
		var value = "";
		var properties = {};

		for(idx = 0; idx < curUrl.length; idx++)
		{
			var c = curUrl.charAt(idx);

			if (getKey)
			{
				if (c == ';')
				{
					parseFailed = true;
					break;
				}
				else if (c == '/')
				{
					parseFailed = true;
					break;
				}
				else if (c == '=')
				{
					getKey = false;
					getValue = true;
				}
				else
				{
					key += c;
				}
			}
			else if (getValue)
			{
				if (c == ';')
				{
					getKey = true;
					getValue = false;
					properties[key] = value;
					key = "";
					value = "";
				}
				else if (c == '"')
				{
					getValue = false;
					getQuoteValue = true;
				}
				else
				{
					value += c;
				}
			}
			else if (getQuoteValue)
			{
				if (escaped)
				{
					escaped = false;
					value += c;
				}
				else if (c == '"')
				{
					getValue = true;
					getQuoteValue = false;
				}
				else if (c == '\\')
				{
					escaped = true;
				}
				else
				{
					value += c;
				}
			}
		}

		if (getQuoteValue)
		{
			return false;
		}
		else if (getValue && key != "" && value != "")
		{
			properties[key] = value;
			key = "";
			value = "";
		}

		for(var key in properties)
		{
			var value = properties[key];

			switch (key.toLowerCase())
			{
				case "databasename":
				{
					if (!outInfo.db == "")
					{
						return false;
					}

					outInfo.db = value;
				}
				break;
				case "user":
				{
					if (!inOutUserNamePassword.userName == "" && inOutUserNamePassword.userName !== null && inOutUserNamePassword.userName != value)
					{
						return false;
					}

					inOutUserNamePassword.userName = value;
				}
				break;
				case "password":
				{
					if (!inOutUserNamePassword.password == "" && inOutUserNamePassword.password !== null && inOutUserNamePassword.password != value)
					{
						return false;
					}

					inOutUserNamePassword.password = value;
				}
				break;
			}
		}

		return true;
	};

	// iterate the drives and put into the mountpoints into the paths list (using a Promise object.)
	var BuildDriveList = function(callback)
	{
		drivelist.list((error, drives) => {
			if(error) {
				return callback(error);
			}
			var paths = [];

			for(var i = 0; i < drives.length; i++)
			{
				if(drives[i].mountpoints !== undefined && drives[i].mountpoints !== null)
				{
					for(var j = 0; j < drives[i].mountpoints.length; j++)
					{
						if(drives[i].mountpoints[j].path !== undefined && drives[i].mountpoints[j].path !== null && drives[i].mountpoints[j].path)
						{
							paths.push(drives[i].mountpoints[j].path);
						}
					}
				}
			}

			return callback(null, paths);
		});

		return undefined;
	};

	var tomcatSearchPaths = [ "var/lib/tomcat/conf/context.xml", "var/lib/tomcat5/conf/conf/context.xml", "var/lib/tomcat6/conf/conf/context.xml", "var/lib/tomcat7/conf/conf/context.xml", "var/lib/tomcat8/conf/conf/context.xml", "var/lib/tomcat9/conf/conf/context.xml", "etc/tomcat/conf/context.xml", "etc/tomcat5/conf/context.xml", "etc/tomcat6/conf/context.xml", "etc/tomcat7/conf/context.xml", "etc/tomcat8/conf/context.xml", "etc/tomcat9/conf/context.xml" ];

	// collect the paths into search paths and whether or not these are tomcat contexts.
	var CollectSearchPaths = function(paths, callback) 
	{
		var searchPaths = [];
		var useTomcatsMap = [];

		var searchPath;
		var useTomcat;
		var pathEndsSlash;

		for(var i = 0; i < paths.length; i++)
		{
			var path = paths[i];
			if(path.endsWith("/") || path.endsWith("\\"))
			{
				pathEndsSlash = true;
			}
			else
			{
				pathEndsSlash = false;
			}

			if(pathEndsSlash)
			{
				searchPath = path + "var/wwwContext/context.xml";
			}
			else
			{
				searchPath = path + "/var/wwwContext/context.xml";
			}
			useTomcat = false;

			searchPaths.push(searchPath);
			useTomcatsMap.push(useTomcat);

			useTomcat = true;
			for(var j = 0; j < tomcatSearchPaths.length; j++)
			{
				if(pathEndsSlash)
				{
					searchPath = path + tomcatSearchPaths[j];
				}
				else
				{
					searchPath = path + "/" + tomcatSearchPaths[j];
				}

				searchPaths.push(searchPath);
				useTomcatsMap.push(useTomcat);
			}
		}
		return callback(null, { "search_paths" : searchPaths, "use_tomcat_parsers": useTomcatsMap});
	};

	// search the search paths for our context.
	var ReadSearchPaths = function(context, result, callback)
	{
		var xpathApache = "/Context/Resource[@name='" + context + "']";
		var xpathTomcat = "/Context/Resource[@name='" + "jdbc/" + context + "']";

		var searchPaths = result.search_paths;
		var useTomcatsMap = result.use_tomcat_parsers;
		var nodeSets = [];
		var numCallbacks = searchPaths.length;
		var numCompletedCallbacks = 0;

		// read the search paths, for our context, into text (using an array of promises).
		// at the same time run xpath to return only the applicable nodes containing our context.
		for(i = 0; i < searchPaths.length; i++)
		{
			var searchPath = searchPaths[i];
			var useTomcat = useTomcatsMap[i];

			var func = function(i, useTomcat, searchPath) {
				fs.readFile(searchPath, "utf8", (err, data) => {
					var idx = i;
					var ut = useTomcat;

					if(err)
					{
						numCompletedCallbacks++;
						if(numCompletedCallbacks == numCallbacks)
						{
							return callback(null, nodeSets);
						}
						return;
					}

					var doc = new dom().parseFromString(data);
					var nodes = [];

					if(doc !== undefined && doc !== null)
					{
						if(ut)
						{
							nodes = xpath.select(xpathTomcat, doc);
						}
						else
						{
							nodes = xpath.select(xpathApache, doc);
						}
						numCompletedCallbacks++;
						nodeSets.push({ 'idx': idx, 'use_tomcat': ut, 'nodes': nodes });
						if(numCompletedCallbacks == numCallbacks)
						{
							return callback(null, nodeSets);
						}
						return;
					}
					else
					{
						numCompletedCallbacks++;
						if(numCompletedCallbacks == numCallbacks)
						{
							return callback(null, nodeSets);
						}
						return;
					}
				});
			};
			func(i, useTomcat, searchPath);
		}
	};

	// parse the individual context xml nodes, and read the connection information from them.
	var GetConnectionInfos = function(thisObj, nodeSets, callback)
	{
		var connInfos = [];

		for(var i = 0; i < nodeSets.length; i++)
		{
			var nodeSet = nodeSets[i];

			if(nodeSet.idx !== undefined)
			{
				var idx = nodeSet.idx;
				var useTomcat = nodeSet.use_tomcat;
				var nodes = nodeSet.nodes;

				if(nodes.length > 0)
				{
					for(var j = 0; j < nodes.length; j++)
					{
						var connInfo = null;
						var node = nodes[j];

						if(useTomcat)
						{
							if(node.hasAttributes())
							{
								var clazz = node.hasAttribute("driverClassName") ? node.getAttribute("driverClassName") : "";
								var userName = node.hasAttribute("username") ? node.getAttribute("username") : "";
								var password = node.hasAttribute("password") ? node.getAttribute("password") : "";
								var url = node.hasAttribute("url") ? node.getAttribute("url") : "";
								var nospaceurl = url + "";
								nospaceurl.replace(/\s+/g,'');
								nospaceurl = nospaceurl.toLowerCase();

								var host = null;
								var port = null;
								var db = null;
								var iport = null;

								switch(clazz.toLowerCase())
								{
									case "com.mysql.jdbc.driver":
									case "org.gjt.mm.mysql.driver":
										if (nospaceurl.startsWith("jdbc:mysql:"))
										{
											var oInfo = { 'host': host, 'port': port, 'db': db };
											if(ParseMysqlUrlString(url, oInfo))
											{
												host = oInfo.host;
												port = oInfo.port;
												db = oInfo.db;

												iport = parseInt(port);

												if(!isNaN(iport) && iport > 0)
												{
													connInfo = { 'idx': idx, 'driverClassName': "mysql", 'server': host, 'database': db, 'userName': userName, 'password': password, 'port': iport };
												}
												else
												{
													connInfo = { 'idx': idx, 'driverClassName': "mysql", 'server': host, 'database': db, 'userName': userName, 'password': password };
												}
												if(connInfo !== undefined && connInfo !== null)
												{
													connInfos.push(connInfo);
												}
											}
										}
										break;
									case "org.postgresql.driver":
										if (nospaceurl.startsWith("jdbc:postgresql:"))
										{
											var oInfo = { 'host': host, 'port': port, 'db': db };
											if(ParsePostgresqlUrlString(url, oInfo))
											{
												host = oInfo.host;
												port = oInfo.port;
												db = oInfo.db;

												iport = parseInt(port);

												if(!isNaN(iport) && iport > 0)
												{
													connInfo = { 'idx': idx, 'driverClassName': "postgresql", 'server': host, 'database': db, 'userName': userName, 'password': password, 'port': iport };
												}
												else
												{
													connInfo = { 'idx': idx, 'driverClassName': "postgresql", 'server': host, 'database': db, 'userName': userName, 'password': password };
												}
												if(connInfo !== undefined && connInfo !== null)
												{
													connInfos.push(connInfo);
												}
											}
										}
										break;
									case "com.microsoft.sqlserver.jdbc.sqlserverdriver":
										if (nospaceurl.startsWith("jdbc:sqlserver:"))
										{
											var oInfo = { 'host': host, 'port': port, 'db': db };
											var inOutUserNamePassword = { 'userName': userName, 'password': password };
										
											if(ParseSqlServerUrlString(url, oInfo, inOutUserNamePassword))
											{
												host = oInfo.host;
												port = oInfo.port;
												db = oInfo.db;
												userName = inOutUserNamePassword.userName;
												password = inOutUserNamePassword.password;

												iport = parseInt(port);

												if(!isNaN(iport) && iport > 0)
												{
													connInfo = { 'idx': idx, 'driverClassName': "sqlserver", 'server': host, 'database': db, 'userName': userName, 'password': password, 'port': iport };
												}
												else
												{
													connInfo = { 'idx': idx, 'driverClassName': "sqlserver", 'server': host, 'database': db, 'userName': userName, 'password': password };
												}
												if(connInfo !== undefined && connInfo !== null)
												{
													connInfos.push(connInfo);
												}
											}
										}
										break;
									default:
										thisObj._class = "Unknown: tomcat: " + clazz;
										break;
								}
							}
						}
						else
						{
							if(node.hasAttributes())
							{
								var clazz = node.hasAttribute("class") ? node.getAttribute("class") : "";
								var userName = node.hasAttribute("username") ? node.getAttribute("username") : "";
								var password = node.hasAttribute("password") ? node.getAttribute("password") : "";
								var host = node.hasAttribute("host") ? node.getAttribute("host") : "";
								var db = node.hasAttribute("db") ? node.getAttribute("db") : "";
								var port = node.hasAttribute("port") ? node.getAttribute("port") : "";
								var iport = parseInt(port);

								if(!isNaN(iport) && iport > 0)
								{
									connInfo = { 'idx': idx, 'driverClassName': clazz, 'server': host, 'database': db, 'userName': userName, 'password': password, 'port': iport };
								}
								else
								{
									connInfo = { 'idx': idx, 'driverClassName': clazz, 'server': host, 'database': db, 'userName': userName, 'password': password };
								}
								if(connInfo !== undefined && connInfo !== null)
								{
									connInfos.push(connInfo);
								}
							}
						}
					}
				}
			}
		}

		return callback(null, connInfos);
	};

	// pick the earliest one on the search path, and try to connect to it.
	var PickConnInfo = function(connInfos, callback) {
		var connInfo = null;
		var minIdx = undefined;

		for(var i = 0; i < connInfos.length; i++)
		{
			if(minIdx === undefined || minIdx > connInfos.idx)
			{
				connInfo = connInfos[i];
				minIdx = connInfo.idx;
			}
		}

		return callback(null, connInfo);
	};

	return {
		'constructor': DBConnection,
		'ConnectWithContext': function(context, callback)
		{
			if(this._connected)
			{
				return false;
			}

			this._connecting = true;
			var thisObj = this;
			if(callback === undefined || callback === null)
			{
				return new Promise((resolve, reject) => {
					return BuildDriveList((error, paths) => {
						if(error)
						{
							return reject(error);
						}

						return CollectSearchPaths(paths, (error, result) => {
							if(error)
							{
								return reject(error);
							}

							return ReadSearchPaths(context, result, (error, nodeSets) => {
								if(error)
								{
									return reject(error);
								}

								return GetConnectionInfos(thisObj, nodeSets, (error, connInfos) => {
									if(error)
									{
										return reject(error);
									}

									return PickConnInfo(connInfos, (error, connInfo) => {
										if(error)
										{
											return reject(error);
										}

										return thisObj.ConnectWithUsernamePasswordServer(connInfo.driverClassName, connInfo.server, connInfo.database, connInfo.userName, connInfo.password, connInfo.port, (error, thisObj) => {
											if(error)
											{
												return reject(error);
											}

											return resolve(thisObj);
										});
									});
								});
							});
						});
					});
				});
			}
			else
			{
				return BuildDriveList((error, paths) => {
					if(error)
					{
						callback(error);
					}

					return CollectSearchPaths(paths, (error, result) => {
						if(error)
						{
							callback(error);
						}

						return ReadSearchPaths(context, result, (error, nodeSets) => {
							if(error)
							{
								callback(error);
							}

							return GetConnectionInfos(thisObj, nodeSets, (error, connInfos) => {
								if(error)
								{
									callback(error);
								}

								return PickConnInfo(connInfos, (error, connInfo) => {
									if(error)
									{
										callback(error);
									}

									return thisObj.ConnectWithUsernamePasswordServer(connInfo.driverClassName, connInfo.server, connInfo.database, connInfo.userName, connInfo.password, connInfo.port, callback);
								});
							});
						});
					});
				});
			}

			return null;
		},

		'ConnectWithUsernamePasswordServer': function(driverClassName, server, database, userName, password, port, callback)
		{
			if(this._connected)
			{
				return false;
			}

			var ConnectWithUsernamePasswordServerImpl = function(thisObj, callback) {
				outClass = null;
				outServer = null;
				outPort = 0;
				outDatabase = null;
				outConn = null;

				switch (driverClassName.toLowerCase())
				{
					case "mysql":
					case "mysqli":
					case "mysqlnd":
					{
						outClass = "mysql";

						if (port !== undefined && port !== null && port > 0)
						{
							outPort = port;
						}
						else
						{
							outPort = 3306;
						}

						outServer = server;
						outDatabase = database;

						outConn = mysql.createConnection({ 'host': server, 'port': outPort, 'database': database, 'user': userName, 'password': password });

						if(outConn === undefined || outConn === null)
						{
							return null;
						}

						outConn.connect(function(err) {
							if(err)
							{
								return callback(err);
							}

							thisObj._clazz = outClass;
							thisObj._server = outServer;
							thisObj._port = outPort;
							thisObj._database = outDatabase;
							thisObj._conn = outConn;
							thisObj._connecting = false;
							thisObj._connected = true;

							// Manually extracted from mysql-5.7.9/include/mysql.h.pp
							// some more info here: http://dev.mysql.com/doc/refman/5.5/en/c-api-prepared-statement-type-codes.html
							thisObj._typesLookup[0x00] = "DECIMAL"; // aka DECIMAL (http://dev.mysql.com/doc/refman/5.0/en/precision-math-decimal-changes.html)
							thisObj._typesLookup[0x01] = "TINY"; // aka TINYINT, 1 byte
							thisObj._typesLookup[0x02] = "SHORT"; // aka SMALLINT, 2 bytes
							thisObj._typesLookup[0x03] = "LONG"; // aka INT, 4 bytes
							thisObj._typesLookup[0x04] = "FLOAT"; // aka FLOAT, 4-8 bytes
							thisObj._typesLookup[0x05] = "DOUBLE"; // aka DOUBLE, 8 bytes
							thisObj._typesLookup[0x06] = "NULL"; // NULL (used for prepared statements, I think)
							thisObj._typesLookup[0x07] = "TIMESTAMP"; // aka TIMESTAMP
							thisObj._typesLookup[0x08] = "LONGLONG"; // aka BIGINT, 8 bytes
							thisObj._typesLookup[0x09] = "INT24"; // aka MEDIUMINT, 3 bytes
							thisObj._typesLookup[0x0a] = "DATE"; // aka DATE
							thisObj._typesLookup[0x0b] = "TIME"; // aka TIME
							thisObj._typesLookup[0x0c] = "DATETIME"; // aka DATETIME
							thisObj._typesLookup[0x0d] = "YEAR"; // aka YEAR, 1 byte (don't ask)
							thisObj._typesLookup[0x0e] = "NEWDATE"; // aka ?
							thisObj._typesLookup[0x0f] = "VARCHAR"; // aka VARCHAR (?)
							thisObj._typesLookup[0x10] = "BIT"; // aka BIT, 1-8 byte
							thisObj._typesLookup[0x11] = "TIMESTAMP2"; // aka TIMESTAMP with fractional seconds
							thisObj._typesLookup[0x12] = "DATETIME2"; // aka DATETIME with fractional seconds
							thisObj._typesLookup[0x13] = "TIME2"; // aka TIME with fractional seconds
							thisObj._typesLookup[0xf5] = "JSON"; // aka JSON
							thisObj._typesLookup[0xf6] = "NEWDECIMAL"; // aka DECIMAL
							thisObj._typesLookup[0xf7] = "ENUM"; // aka ENUM
							thisObj._typesLookup[0xf8] = "SET"; // aka SET
							thisObj._typesLookup[0xf9] = "TINY_BLOB"; // aka TINYBLOB, TINYTEXT
							thisObj._typesLookup[0xfa] = "MEDIUM_BLOB"; // aka MEDIUMBLOB, MEDIUMTEXT
							thisObj._typesLookup[0xfb] = "LONG_BLOB"; // aka LONGBLOG, LONGTEXT
							thisObj._typesLookup[0xfc] = "BLOB"; // aka BLOB, TEXT
							thisObj._typesLookup[0xfd] = "VAR_STRING"; // aka VARCHAR, VARBINARY
							thisObj._typesLookup[0xfe] = "STRING"; // aka CHAR, BINARY
							thisObj._typesLookup[0xff] = "GEOMETRY"; // aka GEOMETRY

							return callback(null, thisObj);
						});
						break;
					}
					case "pgsql":
					case "postgresql":
					case "postgres":
					{
						outClass = "postgresql";
						if (port !== undefined && port !== null && port > 0)
						{
							outPort = port;
						}
						else
						{
							outPort = 5432;
						}
						outServer = server;
						outDatabase = database;

						outConn = new pg.Client({ 'host': server, 'port': outPort, 'database': database, 'user': userName, 'password': password });
						if(outConn === undefined || outConn === null)
						{
							return null;
						}

						outConn.connect(function(err) {
							if(err)
							{
								return callback(err);
							}

							thisObj._clazz = outClass;
							thisObj._server = outServer;
							thisObj._port = outPort;
							thisObj._database = outDatabase;
							thisObj._conn = outConn;
							thisObj._connecting = false;
							thisObj._connected = true;

							var query = { 'text':	"SELECT * " + 
										"FROM ( " + 
											"SELECT dataTypeId.val AS data_type_id, format_type(dataTypeId.val, NULL) AS type_name " + 
											"FROM (SELECT generate_series(1, 65535, 1) AS val) dataTypeId " +
											") AS type_hash " + 
											"WHERE type_hash.type_name != '???';", 'values': []};

							thisObj._conn.query(query, (error, result) => {
								if(error)
								{
									return callback(err);
								}

								for(var i = 0; i < result.rows.length; i++)
								{
									var row = result.rows[i];
									thisObj._typesLookup[row.data_type_id] = row.type_name;
								}
								return callback(null, result);
							});
							return;
						});
						break;
					}
					case "sqlserver":
					case "sqlsrv":
					{
						outClass = "sqlserver";
						if (port !== undefined && port !== null && port == 0)
						{
							outPort = 1433;
						}
						else
						{
							outPort = port;
						}
						outServer = server;
						outDatabase = database;

						outConn = new mssql.ConnectionPool({ 'server': server, 'port': outPort, 'database': database, 'user': userName, 'password': password });

						if(outConn === undefined || outConn === null)
						{
							return null;
						}

						outConn.connect(function(err) {
							if(err)
							{
								return callback(err);
							}

							thisObj._clazz = outClass;
							thisObj._server = outServer;
							thisObj._port = outPort;
							thisObj._database = outDatabase;
							thisObj._conn = outConn;
							thisObj._connecting = false;
							thisObj._connected = true;

							thisObj._typesLookup["VarChar"] = mssql.TYPES.VarChar;
							thisObj._typesLookup["NVarChar"] = mssql.TYPES.NVarChar;
							thisObj._typesLookup["Text"] = mssql.TYPES.Text;
							thisObj._typesLookup["Int"] = mssql.TYPES.Int;
							thisObj._typesLookup["BigInt"] = mssql.TYPES.BigInt;
							thisObj._typesLookup["TinyInt"] = mssql.TYPES.TinyInt;
							thisObj._typesLookup["SmallInt"] = mssql.TYPES.SmallInt;
							thisObj._typesLookup["Bit"] = mssql.TYPES.Bit;
							thisObj._typesLookup["Float"] = mssql.TYPES.Float;
							thisObj._typesLookup["Numeric"] = mssql.TYPES.Numeric;
							thisObj._typesLookup["Decimal"] = mssql.TYPES.Decimal;
							thisObj._typesLookup["Real"] = mssql.TYPES.Real;
							thisObj._typesLookup["Date"] = mssql.TYPES.Date;
							thisObj._typesLookup["DateTime"] = mssql.TYPES.DateTime;
							thisObj._typesLookup["DateTime2"] = mssql.TYPES.DateTime2;
							thisObj._typesLookup["DateTimeOffset"] = mssql.TYPES.DateTimeOffset;
							thisObj._typesLookup["SmallDateTime"] = mssql.TYPES.SmallDateTime;
							thisObj._typesLookup["Time"] = mssql.TYPES.Time;
							thisObj._typesLookup["UniqueIdentifier"] = mssql.TYPES.UniqueIdentifier;
							thisObj._typesLookup["SmallMoney"] = mssql.TYPES.SmallMoney;
							thisObj._typesLookup["Money"] = mssql.TYPES.Money;
							thisObj._typesLookup["Binary"] = mssql.TYPES.Binary;
							thisObj._typesLookup["VarBinary"] = mssql.TYPES.VarBinary;
							thisObj._typesLookup["Image"] = mssql.TYPES.Image;
							thisObj._typesLookup["Xml"] = mssql.TYPES.Xml;
							thisObj._typesLookup["Char"] = mssql.TYPES.Char;
							thisObj._typesLookup["NChar"] = mssql.TYPES.NChar;
							thisObj._typesLookup["NText"] = mssql.TYPES.NText;
							thisObj._typesLookup["TVP"] = mssql.TYPES.TVP;
							thisObj._typesLookup["UDT"] = mssql.TYPES.UDT;
							thisObj._typesLookup["Geography"] = mssql.TYPES.Geography;
							thisObj._typesLookup["Geometry"] = mssql.TYPES.Geometry;
							thisObj._typesLookup["Variant"] = mssql.TYPES.Variant;

							return callback(null, thisObj);
						});
						break;
					}
					default:
						outClass = "unknown: " + driverClassName;
						thisObj._clazz = outClass;
						return callback(new Error(outClass));
				}
			}

			this._connecting = true;
			if(callback === undefined || callback === null)
			{
				return new Promise((resolve, reject) => {
					return ConnectWithUsernamePasswordServerImpl(this, (error, thisPtr) => {
						if(error)
						{
							return reject(error);
						}

						return resolve(null, thisPtr);
					});
				});
			}
			else
			{
				return ConnectWithUsernamePasswordServerImpl(this, (error, thisPtr) => {
					if(error)
					{
						return callback(error);
					}

					return callback(null, thisPtr);
				});
			}
			return null;
		},

		'IsConnected': function() {
			return this._connected;
		},

		'DriverClass': function() {
			return this._clazz;
		},

		'Server': function() {
			return this._server;
		},

		'Port': function() {
			return this._port;
		},

		'Database': function() {
			return this._database;
		},

		'PrepareStatement': function(stmt, parameters, callback) {
			var doPrep = function(thisObj, stmt, parameters, callback) {
				if(thisObj._connecting || !thisObj._connected)
				{
					return callback(new Error("not connected yet!"));
				}

				var prepStmt = null;
				var done = false;

				if(stmt != null)
				{
					prepStmt = new PreparedStatement(thisObj, stmt, parameters, (err, tObj) => {
						return callback(null, tObj);
					});
				}
			}

			if(callback === undefined || callback === null)
			{
				return new Promise((resolve, reject) => {
					var thisObj = this;
					return doPrep(thisObj, stmt, parameters, (err, tObj) => {
						if(err)
						{
							return reject(err);
						}

						return resolve(tObj);
					});
				});
			}
			else
			{
				return doPrep(this, stmt, parameters, callback);
			}

			return;
		},

		'ExecuteQuery': function(stmt, parameters, callback)
		{
			var doExecuteQuery = function(thisObj, stmt, parameters, callback) {
				if(thisObj._connecting || !thisObj._connected)
				{
					return callback(new Error("not connected yet!"));
				}

				return thisObj.PrepareStatement(stmt, parameters, (err, preparedStatement) => {
					if(err)
					{
						return callback(err);
					}

					var prepStmt = preparedStatement;

					prepStmt.ExecuteQuery((err, results) => {
						if(err)
						{
							return callback(err);
						}

						var names = [];
						var fieldCount = results.fieldCount();

						for(var i = 0; i < fieldCount; i++)
						{
							names.push(results.fetchFieldMetaData(i).Name);
						}

						var res = [];
						while(results.MoveNext())
						{
							var row = {};
							for(var i = 0; i < fieldCount; i++)
							{
								row[names[i]] = results.fetchFieldData(i);
							}

							res.push(row);
						}

						return prepStmt.close((err, tObj) => {
							if(err)
							{
								return callback(err);
							}

							return callback(null, res);
						});
					});
				});
			};

			if(callback === undefined || callback === null)
			{
				return new Promise((resolve, reject) => {
					var thisObj = this;
					return doExecuteQuery(thisObj, stmt, parameters, (err, results) => {
						if(err)
						{
							return reject(err);
						}

						return resolve(tObj);
					});
				});
			}
			else
			{
				return doExecuteQuery(this, stmt, parameters, callback);
			}

		},

		'ExecuteUpdate': function(stmt, parameters, callback)
		{
			var doExecuteUpdate = function(thisObj, stmt, parameters, callback) {
				if(thisObj._connecting || !thisObj._connected)
				{
					return callback(new Error("not connected yet!"));
				}

				return thisObj.PrepareStatement(stmt, parameters, (err, preparedStatement) => {
					if(err)
					{
						return callback(err);
					}

					var prepStmt = preparedStatement;

					prepStmt.ExecuteUpdate((err, results) => {
						if(err)
						{
							return callback(err);
						}

						return prepStmt.close((err, tObj) => {
							if(err)
							{
								return callback(err);
							}

							return callback(null, results);
						});
					});
				});
			};

			if(callback === undefined || callback === null)
			{
				return new Promise((resolve, reject) => {
					var thisObj = this;
					return doExecuteUpdate(thisObj, stmt, parameters, (err, results) => {
						if(err)
						{
							return reject(err);
						}

						return resolve(tObj);
					});
				});
			}
			else
			{
				return doExecuteUpdate(this, stmt, parameters, callback);
			}

		},

		'ExecuteInsert': function(stmt, parameters, callback)
		{
			var doExecuteInsert = function(thisObj, stmt, parameters, callback) {
				if(thisObj._connecting || !thisObj._connected)
				{
					return callback(new Error("not connected yet!"));
				}

				return thisObj.PrepareStatement(stmt, parameters, (err, preparedStatement) => {
					if(err)
					{
						return callback(err);
					}

					var prepStmt = preparedStatement;

					prepStmt.ExecuteInsert((err, results) => {
						if(err)
						{
							return callback(err);
						}

						return prepStmt.close((err, tObj) => {
							if(err)
							{
								return callback(err);
							}

							return callback(null, results);
						});
					});
				});
			};

			if(callback === undefined || callback === null)
			{
				return new Promise((resolve, reject) => {
					var thisObj = this;
					return doExecuteInsert(thisObj, stmt, parameters, (err, results) => {
						if(err)
						{
							return reject(err);
						}

						return resolve(tObj);
					});
				});
			}
			else
			{
				return doExecuteInsert(this, stmt, parameters, callback);
			}

		},

		'close': function(callback) {
			if(this._connected)
			{
				var closeConn = function(thisObj, i, callback) {
					if(i >= 0)
					{
						return thisObj._preparedStatements[i].close((err, tObj) => {
							if(err)
							{
								return callback(err);
							}

							i--;

							if(i >= 0)
							{
								closeConn(thisObj, i, callback);
							}
							else
							{
								return callback(null, thisObj);
							}
						});
					}
					else
					{
						return callback(null, thisObj);
					}
				};

				var doClose = function(thisObj, callback) {
					var i = thisObj._preparedStatements.length - 1;

					return closeConn(thisObj, i, (err, tObj) => {
						if(err)
						{
							return callback(err);
						}

						switch (thisObj._clazz.toLowerCase())
						{
							case "mysql":
							case "mysqli":
							case "mysqlnd":
							{
								thisObj._conn.end((err) => {
									if(err)
									{
										return callback(err);
									}

									thisObj._conn = null;
									thisObj._connected = false;
									return callback(null, tObj);
								});

								return;
							}
							case "pgsql":
							case "postgresql":
							case "postgres":
							{
								thisObj._conn.end((err) => {
									if(err)
									{
										return callback(err);
									}

									thisObj._conn = null;
									thisObj._connected = false;
									return callback(null, tObj);
								});

								return;
							}
							case "sqlserver":
							case "sqlsrv":
							{
								thisObj._conn.close();
								thisObj._conn = null;
								thisObj._connected = false;
								return callback(null, tObj);
							}
							default:
								outClass = "unknown: " + thisObj._clazz;
								return callback(new Error(outClass));
						}
					});
				};

				if(callback === undefined || callback === null)
				{
					return new Promise((resolve, reject) => {
						var thisObj = this;
						return doClose(thisObj, (err, tObj) => {
							if(err)
							{
								return reject(err);
							}

							return resolve(tObj);
						});
					});
				}
				else
				{
					return doClose(this, callback);
				}
			}
		}
	}
})();

module.exports = DBConnection;
