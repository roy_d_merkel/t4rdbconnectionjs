//  PreparedStatement.js
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

var mssql = require('mssql');
var ResultSet = require("./ResultSet.js");

function PreparedStatement(conn, stmt, params, callback) {
	this._conn = null;
	this._stmt = null;
	this._params = null;
	this._prepStmt = null;
	this._preparing = false;
	this._prepared = false;
	this._resultSets = [];

	var done = false;

	if(conn === undefined || conn === null)
	{
		throw new Exception("Prepared Statemnt called with null or undefined DBConnection object!");
	}

	this._conn = conn;
	var thisObj = this;
	var prepStmt = this.prepare(stmt, params, callback);
}

PreparedStatement.Types = {
	'Bit': 0,
	'BigInt': 1,
	'Decimal': 2,
	'Float': 3,
	'Int': 4,
	'Money': 5,
	'Numeric': 6,
	'SmallInt': 7,
	'SmallMoney': 8,
	'Real': 9,
	'TinyInt': 10,

	'Char': 11,
	'NChar': 12,
	'Text': 13,
	'NText': 14,
	'VarChar': 15,
	'NVarChar': 16,
	'Xml': 17,

	'Time': 18,
	'Date': 19,
	'DateTime': 20,
	'DateTime2': 21,
	'DateTimeOffset': 22,
	'SmallDateTime': 23,

	'UniqueIdentifier': 24,

	'Variant': 25,

	'Binary': 26,
	'VarBinary': 27,
	'Image': 28,

	'UDT': 29,
	'Geography': 30,
	'Geometry': 31
};

PreparedStatement.prototype = (function() {
	var isArray = function(a) {
		    return (!!a) && (a.constructor === Array);
	};

	var arrayRemove = function(arr, item) {
		for(var i = arr.length; i--;) {
			if(arr[i] === item) {
				arr.splice(i, 1);
			}
		}
	};

	return {
		'constructor': PreparedStatement,
		'close': function(callback) {
			if(this._preparing)
			{
				return;
			}

			if(!this._prepared)
			{
				return;
			}

			var closeResultSet = function(thisObj, i, callback) {
				if(i >= 0 && i < thisObj._resultSets.length)
				{
					return thisObj._resultSets[i].Close((err, tObj) => {
						if(err)
						{
							return callback(err);
						}

						i--;

						if(i >= 0)
						{
							closeResultSet(thisObj, i, callback);
						}
						else
						{
							return callback(null, thisObj);
						}
					});
				}
				else
				{
					return callback(null, thisObj);
				}
			};

			var doUnprepare = function(thisObj, callback) {
				var clazz = thisObj._conn.DriverClass();

				switch (clazz.toLowerCase())
				{
					case "mysql":
					case "mysqli":
					case "mysqlnd":
					{
						arrayRemove(thisObj._conn._preparedStatements, thisObj);
						thisObj._prepared = false;
						thisObj._prepStmt = null;
						return callback(null, thisObj);
					}
					case "pgsql":
					case "postgresql":
					case "postgres":
					{
						arrayRemove(thisObj._conn._preparedStatements, thisObj);
						thisObj._prepared = false;
						thisObj._prepStmt = null;
						return callback(null, thisObj);
					}
					case "sqlserver":
					case "sqlsrv":
					{
						thisObj._prepStmt.unprepare((err) => {
							if(err)
							{
								return callback(err);
							}

							arrayRemove(thisObj._conn._preparedStatements, thisObj);
							thisObj._prepared = false;
							thisObj._prepStmt = null;
							return callback(null, thisObj);
						});
						return;
					}
					default:
						return callback(new Error("Unrecognized class: " + clazz));
				}
			};

			if(callback === undefined || callback === null)
			{
				return new Promise((resolve, reject) => {
					var thisObj = this;
					return closeResultSet(thisObj, 0, (err, tObj) => {
						if(err)
						{
							return reject(err);
						}

						return doUnprepare(thisObj, (err, tObj) => {
							if(err)
							{
								return reject(err);
							}

							return resolve(thisObj);
						});
					});
				});
			}
			else
			{
				var thisObj = this;
				return closeResultSet(thisObj, 0, (err, tObj) => {
					if(err)
					{
						return callback(err);
					}

					return doUnprepare(thisObj, (err, thisObj) => {
						if(err)
						{
							return callback(err);
						}

						return callback(null, thisObj);
					});
				});
			}
		},
		// params is an array of arrays in the form [Types.<type>, value, [[length] | [Scale] | [precision], [scale]]]
		'prepare': function(stmt, params, callback) {
			if(stmt === undefined || stmt === null || params === undefined || params === null || !isArray(params))
			{
				return null;
			}

			if(this._preparing)
			{
				return null;
			}

			var runPrep = function(thisObj, stmt, params, callback) {
				var prepStmt = null;
				var clazz = thisObj._conn.DriverClass();

				switch (clazz.toLowerCase())
				{
					case "mysql":
					case "mysqli":
					case "mysqlnd":
					{
						thisObj._prepared = true;
						thisObj._stmt = stmt;
						thisObj._params = params;
						thisObj._conn._preparedStatements.push(thisObj);
						return callback(null, thisObj);
					}
					case "pgsql":
					case "postgresql":
					case "postgres":
					{
						thisObj._prepared = true;
						thisObj._stmt = stmt;
						thisObj._params = params;
						thisObj._prepStmt = null;
						thisObj._conn._preparedStatements.push(thisObj);
						return callback(null, thisObj);
					}
					case "sqlserver":
					case "sqlsrv":
					{
						// TODO: for transactions you can pass a transaction instead of this._conn...
						prepStmt = new mssql.PreparedStatement(thisObj._conn._conn);
						thisObj._prepared = false;
						thisObj._preparing = false;

						if(prepStmt !== undefined && prepStmt !== null)
						{
							thisObj._preparing = true;

							for(var i = 0; i < params.length; i++)
							{
								var param = params[i];

								switch(param[0])
								{
									case thisObj.Types.Bit:
										prepStmt.input("P" + (i + 1), mssql.Bit);
										break;
									case thisObj.Types.BigInt:
										prepStmt.input("P" + (i + 1), mssql.BigInt);
										break;
									case thisObj.Types.Decimal:
										if(param[2] !== undefined && param[2] != null && param[1] !== undefined && param[1] !== null)
										{
											prepStmt.input("P" + (i + 1), mssql.Decimal(param[1], param[2]));
										}
										else if(param[1] !== undefined && param[1] !== null)
										{
											prepStmt.input("P" + (i + 1), mssql.Decimal(param[1]));
										}
										else
										{
											prepStmt.input("P" + (i + 1), mssql.Decimal);
										}
										break;
									case thisObj.Types.Float:
										prepStmt.input("P" + (i + 1), mssql.Float);
										break;
									case thisObj.Types.Int:
										prepStmt.input("P" + (i + 1), mssql.Int);
										break;
									case thisObj.Types.Money:
										prepStmt.input("P" + (i + 1), mssql.Money);
										break;
									case thisObj.Types.Numeric:
										if(param[2] !== undefined && param[2] != null && param[1] !== undefined && param[1] !== null)
										{
											prepStmt.input("P" + (i + 1), mssql.Numeric(param[1], param[2]));
										}
										else if(param[1] !== undefined && param[1] !== null)
										{
											prepStmt.input("P" + (i + 1), mssql.Numeric(param[1]));
										}
										else
										{
											prepStmt.input("P" + (i + 1), mssql.Numeric);
										}
										break;
									case thisObj.Types.SmallInt:
										prepStmt.input("P" + (i + 1), mssql.SmallInt);
										break;
									case thisObj.Types.SmallMoney:
										prepStmt.input("P" + (i + 1), mssql.SmallMoney);
										break;
									case thisObj.Types.Real:
										prepStmt.input("P" + (i + 1), mssql.Real);
										break;
									case thisObj.Types.TinyInt:
										prepStmt.input("P" + (i + 1), mssql.TinyInt);
										break;
									case thisObj.Types.Char:
										if(param[1] !== undefined && param[1] !== null)
										{
											prepStmt.input("P" + (i + 1), mssql.Char(param[1]));
										}
										else
										{
											prepStmt.input("P" + (i + 1), mssql.Char);
										}
										break;
									case thisObj.Types.NChar:
										if(param[1] !== undefined && param[1] !== null)
										{
											prepStmt.input("P" + (i + 1), mssql.NChar(param[1]));
										}
										else
										{
											prepStmt.input("P" + (i + 1), mssql.NChar);
										}
										break;
									case thisObj.Types.Text:
										prepStmt.input("P" + (i + 1), mssql.Text);
										break;
									case thisObj.Types.NText:
										prepStmt.input("P" + (i + 1), mssql.NText);
										break;
									case thisObj.Types.VarChar:
										if(param[1] !== undefined && param[1] !== null)
										{
											prepStmt.input("P" + (i + 1), mssql.VarChar(param[1]));
										}
										else
										{
											prepStmt.input("P" + (i + 1), mssql.VarChar);
										}
										break;
									case thisObj.Types.NVarChar:
										if(param[1] !== undefined && param[1] !== null)
										{
											prepStmt.input("P" + (i + 1), mssql.NVarChar(param[1]));
										}
										else
										{
											prepStmt.input("P" + (i + 1), mssql.NVarChar);
										}
										break;
									case thisObj.Types.Xml:
										prepStmt.input("P" + (i + 1), mssql.Xml);
										break;
									case thisObj.Types.Time:
										if(param[1] !== undefined && param[1] !== null)
										{
											prepStmt.input("P" + (i + 1), mssql.Time(param[1]));
										}
										else
										{
											prepStmt.input("P" + (i + 1), mssql.Time);
										}
										break;
									case thisObj.Types.Date:
										prepStmt.input("P" + (i + 1), mssql.Date);
										break;
									case thisObj.Types.DateTime:
										prepStmt.input("P" + (i + 1), mssql.DateTime);
										break;
									case thisObj.Types.DateTime2:
										prepStmt.input("P" + (i + 1), mssql.DateTime2);
										break;
									case thisObj.Types.DateTimeOffset:
										prepStmt.input("P" + (i + 1), mssql.DateTimeOffset);
										break;
									case thisObj.Types.SmallDateTime:
										prepStmt.input("P" + (i + 1), mssql.SmallDateTime);
										break;
									case thisObj.Types.UniqueIdentifier:
										prepStmt.input("P" + (i + 1), mssql.UniqueIdentifier);
										break;
									case thisObj.Types.Variant:
										prepStmt.input("P" + (i + 1), mssql.Variant);
										break;

									case thisObj.Types.Binary:
										prepStmt.input("P" + (i + 1), mssql.Binary);
										break;
									case thisObj.Types.VarBinary:
										if(param[1] !== undefined && param[1] !== null)
										{
											prepStmt.input("P" + (i + 1), mssql.VarBinary(param[1]));
										}
										else
										{
											prepStmt.input("P" + (i + 1), mssql.VarBinary);
										}
										break;
									case thisObj.Types.Image:
										prepStmt.input("P" + (i + 1), mssql.Image);
										break;

									case thisObj.Types.UDT:
										prepStmt.input("P" + (i + 1), mssql.UDT);
										break;
									case thisObj.Types.Geography:
										prepStmt.input("P" + (i + 1), mssql.Geography);
										break;
									case thisObj.Types.Geometry:
										prepStmt.input("P" + (i + 1), mssql.Geometry);
										break;
								}
							}

							prepStmt.prepare(stmt, (err) => {
								thisObj._preparing = false;
								if(err)
								{
									return callback(err);
								}

								prepStmt.stream = true;
								thisObj._stmt = stmt;
								thisObj._params = params;
								thisObj._prepStmt = prepStmt;
								thisObj._prepared = true;

								thisObj._conn._preparedStatements.push(thisObj);
								return callback(null, thisObj);
							});

							return;
						}
						else
						{
							return callback(new Error("Failed to construct PreparedStatement object."));
						}

						break;
					}
					default:
						return callback(new Error("Unrecognized class: " + clazz));
				}
			};

			if(callback === undefined || callback === null)
			{
				if(this._prepared)
				{
					return new Promise((resolve, reject) => {
						var thisObj = this;
						return thisObj.close((err, tObj) => {
							if(err)
							{
								return reject(err);
							}

							return runPrep(thisObj, stmt, params, (err, tObj) => {
								if(err)
								{
									return reject(err);
								}

								return resolve(thisObj);
							});
						});
					});
				}
				else
				{
					return new Promise((resolve, reject) => {
						var thisObj = this;
						return runPrep(thisObj, stmt, params, (err, tObj) => {
							if(err)
							{
								return reject(err);
							}

							return resolve(thisObj);
						});
					});
				}
			}
			else
			{
				if(this._prepared)
				{
					var thisObj= this;
					return this.close((err, tObj) => {
						if(err)
						{
							return callback(err);
						}

						return runPrep(thisObj, stmt, params, (err, tObj) => {
							if(err)
							{
								return callback(err);
							}

							return callback(null, tObj);
						});
					});
				}
				else
				{
					return runPrep(this, stmt, params, (err, thisObj) => {
						if(err)
						{
							return callback(err);
						}

						return callback(null, thisObj);
					});
				}
			}
		},
		'ExecuteQuery': function(callback) {
			return new ResultSet(this, callback);
		},
		'ExecuteUpdate' : function(callback) {
			if(!this._prepared)
			{
				return callback(new Error("Prepared Statement hasn't finished being prepared yet, or was never prepared."));
			}

			var clazz = this._conn.DriverClass();

			switch (clazz.toLowerCase())
			{
				case "mysql":
				case "mysqli":
				case "mysqlnd":
				{
					var params = [];

					for(var i = 0; i < this._params.length; i++)
					{
					params[i] = this._params[i][1];
					}

					var thisObj = this;
					this._conn._conn.query(this._stmt, params, (error, results, fields) => {
						if(error)
						{
							return callback(error);
						}

						var res = 0;

						for(var j = 0; j < results.length; j++)
						{
							if(results[j].affectedRows !== undefined && results[j].affectedRows !== null)
							{
								res = results[j].affectedRows;
							}
						}

						return callback(null, res);
					});

					return;
				}
				case "pgsql":
				case "postgresql":
				case "postgres":
				{
					var params = [];

					for(var i = 0; i < this._params.length; i++)
					{
						params[i] = this._params[i][1];
					}

					var query = { 'text': this._stmt, 'values': params };

					var thisObj = this;
					this._conn._conn.query(query, (error, result) => {
						if(error)
						{
							return callback(error);
						}

						return callback(null, result.rowCount);
					});

					return;
				}
				case "sqlserver":
				case "sqlsrv":
				{
					var error = false;
					var thisObj = this;
					var hasRecordSet = false;
					var args = {};
					for(var i = 0; i < this._params.length; i++)
					{
						args["P" + (i + 1)] = (this._params)[i][1]
					}

					this._prepStmt.stream = false;
					var request = this._prepStmt.execute(args, (err, result) => {
						thisObj._prepStmt.stream = true;
						if(err)
						{
							console.log(err);
							return callback(err);
						}
						return callback(null, result.rowsAffected[0]);
					});

					return;
				}
				default:
					return callback(new Error("Unrecognized class: " + clazz));
			}
		},
		'ExecuteInsert' : function(callback) {
			// TODO: figure out how to return the keys inserted.
			if(!this._prepared)
			{
				return callback(new Error("Prepared Statement hasn't finished being prepared yet, or was never prepared."));
			}

			var clazz = this._conn.DriverClass();

			switch (clazz.toLowerCase())
			{
				case "mysql":
				case "mysqli":
				case "mysqlnd":
				{
					var params = [];

					for(var i = 0; i < this._params.length; i++)
					{
					params[i] = this._params[i][1];
					}

					var thisObj = this;
					this._conn._conn.query(this._stmt, params, (error, results, fields) => {
						if(error)
						{
							return callback(error);
						}

						return callback(null, results.affectedRows);
					});

					return;
				}
				case "pgsql":
				case "postgresql":
				case "postgres":
				{
					var params = [];

					for(var i = 0; i < this._params.length; i++)
					{
						params[i] = this._params[i][1];
					}

					var query = { 'text': this._stmt, 'values': params };

					var thisObj = this;
					this._conn._conn.query(query, (error, result) => {
						if(error)
						{
							return callback(error);
						}

						return callback(null, result.rowCount);
					});

					return;
				}
				case "sqlserver":
				case "sqlsrv":
				{
					var error = false;
					var thisObj = this;
					var hasRecordSet = false;
					var args = {};
					for(var i = 0; i < this._params.length; i++)
					{
						args["P" + (i + 1)] = (this._params)[i][1]
					}

					this._prepStmt.stream = false;
					var request = this._prepStmt.execute(args, (err, result) => {
						thisObj._prepStmt.stream = true;
						if(err)
						{
							console.log(err);
							return callback(err);
						}
						return callback(null, result.rowsAffected[0]);
					});

					return;
				}
				default:
					return callback(new Error("Unrecognized class: " + clazz));
			}
		},
		'Types': PreparedStatement.Types
	}
})();

module.exports = PreparedStatement;
