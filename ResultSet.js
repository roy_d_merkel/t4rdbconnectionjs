//  ResultSet.js
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

function ResultSet(stmt, callback) {
	// TODO: find a way to keep java style ResultSet.next and still utilize streaming callbacks
	var isArray = function(a) {
		    return (!!a) && (a.constructor === Array);
	};

	this._stmt = stmt;
	this._columns = [];
	this._metaData = [];
	this._fieldCount = 0;
	this._rows = [];
	this._curRow = -1;
	this._fetching = false;
	this._fetched = true;

	if(!this._stmt._prepared)
	{
		return callback(new Error("Prepared Statement hasn't finished being prepared yet, or was never prepared."));
	}

	this._fetching = true;
	var clazz = this._stmt._conn.DriverClass();

	switch (clazz.toLowerCase())
	{
		case "mysql":
		case "mysqli":
		case "mysqlnd":
		{
			var params = [];

			for(var i = 0; i < this._stmt._params.length; i++)
			{
				params[i] = this._stmt._params[i][1];
			}

			var thisObj = this;
			this._stmt._conn._conn.query(this._stmt._stmt, params, (error, results, fields) => {
				if(error)
				{
					return callback(error);
				}

				var fieldIdxLookup = [];

				thisObj._fieldCount = fields.length;
				for(var i = 0; i < fields.length; i++)
				{
					var field = fields[i];

					var name = field.name;
					var type = field.type;

					fieldIdxLookup[name] = i;
					thisObj._metaData[i] = { 'Name': name, 'Type': thisObj._stmt._conn._typesLookup[type] };
				}

				for(var i = 0; i < results.length; i++)
				{
					var row = results[i];
					var r = [];

					for(var key in row)
					{
						var idx = fieldIdxLookup[key];
						var value = row[key];

						r[idx] = value;
					}

					thisObj._rows.push(r);
				}

				thisObj._fetching = false;
				thisObj._fetched = true;
				thisObj._stmt._resultSets.push(thisObj);
				return callback(null, thisObj);
			});
			return;
		}
		case "pgsql":
		case "postgresql":
		case "postgres":
		{
			var params = [];

			for(var i = 0; i < this._stmt._params.length; i++)
			{
				params[i] = this._stmt._params[i][1];
			}

			var query = { 'text': this._stmt._stmt, 'values': params, 'rowMode': 'array' };

			var thisObj = this;
			this._stmt._conn._conn.query(query, (error, result) => {
				if(error)
				{
					return callback(error);
				}

				thisObj._fieldCount = result.fields.length;
				for(var i = 0; i < thisObj._fieldCount; i++)
				{
					var field = result.fields[i];

					var name = field.name;
					var type = field.dataTypeID;

					thisObj._metaData[i] = { 'Name': name, 'Type': thisObj._stmt._conn._typesLookup[type] };
				}

				for(var i = 0; i < result.rows.length; i++)
				{
					thisObj._rows.push(result.rows[i]);
				}

				thisObj._fetching = false;
				thisObj._fetched = true;
				thisObj._stmt._resultSets.push(thisObj);
				return callback(null, result);
			});

			return;
		}
		case "sqlserver":
		case "sqlsrv":
		{
			var error = false;
			var thisObj = this;
			var args = {};
			for(var i = 0; i < this._stmt._params.length; i++)
			{
				args["P" + (i + 1)] = (this._stmt._params)[i][1]
			}

			var request = this._stmt._prepStmt.execute(args, (err, result) => {
				// NOOP! This is required to prevent a warning, it's contents are intersperced below.
			});

			request.on('recordset', columns => {
				// Emitted once for each recordset in a query
				if(thisObj._metaData.length > 0)
				{
					error = true;
					return callback(new Error("Only one recordset per prepared statement is supported, at this time."));
				}

				var keys = Object.keys(columns);
				thisObj._columns = keys;
				thisObj._fieldCount = thisObj._columns.length;
				thisObj._metaData = new Array(thisObj._fieldCount);
				var minIdx = -1;
				// fill in holes for duplicates that start array, e.g. SELECT 1, 2 AS A, 3, 4 AS B
				// will have metadata: [ { 'name': '', 'index': 2 }, ... ] instead of index : 0
				// so we need to handle this...
				for(var i = 0; i < keys.length; i++)
				{
					var key = keys[i];
					var value = columns[key];

					var index = value.index;

					if(minIdx < 0 || index < minIdx)
					{
						minIdx = index;
					}
				}
				for(var i = 0; i < keys.length; i++)
				{
					var key = keys[i];
					var value = columns[key];

					var index = value.index;
					var name = value.name;
					var type = value.type;

					for(var typeName in thisObj._stmt._conn._typesLookup)
					{
						var typeKey = thisObj._stmt._conn._typesLookup[typeName];

						if(typeKey == type)
						{
							type = typeName;
							break;
						}
					}
					thisObj._metaData[index - minIdx] = { 'Name': name, 'Type': type };
				}
			});

			request.on('row', row => {
				// Emitted for each row in a recordset
				if(error)
				{
					return;
				}

				var r = [];

				for(var key in row)
				{
					var value = row[key];
					var idx = -1;

					for(var i = 0; i < thisObj._fieldCount; i++)
					{
						if(thisObj._metaData[i].Name == key)
						{
							idx = i;
							break;
						}
					}

					if(idx < 0)
					{
						error = true;
						return callback(new Error("column in rows that isn't in recordset metadata: '" + key + "'"));
					}
					else
					{
						if(isArray(value))
						{
							error = true;
							return callback(new Error("the column '" + key + "' is in the recordset more then once, this is not currently supported."));
						}
						r[idx] = value;
					}
				}

				thisObj._rows.push(r);
			});

			request.on('error', err => {
				// May be emitted multiple times
				if(error)
				{
					return;
				}
				error = true;
				return callback(err);
			});

			request.on('done', result => {
				// Always emitted as the last one
				// resultset: { output: { ... }, rowsAffected: [ ... ], returnValue: 0 }
				if(error)
				{
					return;
				}

				thisObj._fetching = false;
				thisObj._fetched = true;
				thisObj._stmt._resultSets.push(thisObj);
				return callback(null, thisObj);
			});

			return;
		}
		default:
			thisObj._fetching = false;
			thisObj._fetched = false;
			return callback(new Error("Unrecognized class: " + clazz));
	}
}

ResultSet.prototype = (function() {
	var arrayRemove = function(arr, item) {
		for(var i = arr.length; i--;) {
			if(arr[i] === item) {
				arr.splice(i, 1);
			}
		}
	};

	return {
		'constructor': ResultSet,
		// returns 0 on invalid or no fields or the field count.
		'fieldCount': function()
		{
			if(this._fetching || !this._fetched)
			{
				return 0;
			}

			return this._fieldCount;
		},
		// fetches null or the field metadata.
		'fetchFieldMetaData': function(i)
		{
			if(this._fetching || !this._fetched)
			{
				return null;
			}

			if(this._metaData[i] !== undefined)
			{
				return this._metaData[i];
			}
		},
		// fetches null or the field's value.
		'fetchFieldData': function(i)
		{
			if(this._fetching || !this._fetched)
			{
				return null;
			}

			if(this._curRow >= 0 && this._curRow < this._rows.length)
			{
				if(i >= 0 && i < this._metaData.length)
				{
					return this._rows[this._curRow][i];
				}
			}
			return null;
		},
		'MoveNext': function()
		{
			if(this._fetching || !this._fetched)
			{
				return false;
			}

			this._curRow++;

			return (this._curRow >= 0 && this._curRow < this._rows.length);
		},
		'Close': function(callback)
		{
			this._fetching = false;
			this._fetched = false;
			this._metaData = [];
			this._rows = [];
			arrayRemove(this._stmt._resultSets, this);

			return callback(null, this);
		}
	}
})();

module.exports = ResultSet;
